FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI = "git://gitlab.cern.ch/Caribou/firmware/peary-firmware-clicpix2.git;branch=desy;protocol=https; \
           file://0001-Add-I2C-muxes-of-the-CaR-board-to-the-device-tree.patch;striplevel=3; \
          "

SYSTEM_USER_DTSI ?= "Caribou-Patches.dtsi"
SRC_URI_append = " file://${SYSTEM_USER_DTSI}"



SRCREV = "${AUTOREV}"

PV = "1.0+git${SRCPV}"

S = "${WORKDIR}/git/outputs/dts/"

KERNEL_DTS_INCLUDE_caribou-zynq7 = "\
			git/outputs/dts \
                        Caribou-Patches.dtsi \
			"

COMPATIBLE_MACHINE_caribou-zynq7 = ".*"


do_configure_append() {
        cp ${WORKDIR}/${SYSTEM_USER_DTSI} ${B}/../git/outputs/dts/
        echo "/include/ \"${SYSTEM_USER_DTSI}\"" >> ${B}/../git/outputs/dts/system-top.dts
}