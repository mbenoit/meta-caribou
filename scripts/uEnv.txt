machine_name=caribou-zynq7
kernel_image=uImage
kernel_load_address=0x2080000
devicetree_image=system-top.dtb
devicetree_load_address=0x2000000
bootargs=earlyprintk console=ttyPS0,115200 root=/dev/mmcblk0p2 rw rootwait
loadkernel=fatload mmc 0 ${kernel_load_address} ${kernel_image}
loaddtb=fatload mmc 0 ${devicetree_load_address} ${devicetree_image}
bootkernel=run loadkernel && run loaddtb && bootm ${kernel_load_address} - ${devicetree_load_address}
uenvcmd=run loadfpga && run bootkernel
bitstream_image=download.bit
bitstream_load_address=0x100000
bitstream_type=loadb
loadfpga=fatload mmc 0 ${bitstream_load_address} ${bitstream_image} && fpga ${bitstream_type} 0 ${bitstream_load_address} ${filesize}
ethaddr=00:0A:35:00:03:00
bootcmd_mmc=run uenvcmd
