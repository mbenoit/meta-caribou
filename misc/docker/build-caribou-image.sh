#!/bin/bash
export GIT_PATH=/home/caribou/poky/meta-caribou

if [[ -v CI ]]; then  #running in CI
    git clone ${CI_REPOSITORY_URL} ${GIT_PATH}
    git --git-dir=${GIT_PATH}/.git --work-tree=${GIT_PATH} checkout ${CI_COMMIT_SHA}
else                  # otherwise, use latest master
    git clone https://gitlab.cern.ch/Caribou/meta-caribou.git ${GIT_PATH}
    export CI_COMMIT_SHA=$( git --git-dir=${GIT_PATH}/.git --work-tree=${GIT_PATH} rev-parse --verify HEAD )
    export IMAGE_PATH=/home/caribou/poky/build/wic
fi

cd /home/caribou/poky

source meta-caribou/scripts/addCaribouLayer.sh GIT_CI
bitbake caribou-image
bitbake wic-tools
wic create sdimage-bootpart -e caribou-image -o ${IMAGE_PATH}
IMAGE=$(ls -1 ${IMAGE_PATH} | tail -1)
mv ${IMAGE_PATH}/${IMAGE} ${IMAGE_PATH}/sdimage-${CI_COMMIT_SHA:0:8}-mmcblk.direct
