MACHINE = "caribou-zynq7"
#MACHINE = "zc706-zynq7"

SUMMARY = "Image for the Caribou DAQ Framework"
PACKAGE_CLASSES ?= "package_deb"

require recipes-extended/images/core-image-full-cmdline.bb

MACHINE_ESSENTIAL_EXTRA_RDEPENDS += "device-tree kernel-module-sc18is602"  
MACHINE_EXTRA_RRECOMMENDS += "kernel-module-ab123"
KERNEL_MODULE_AUTOLOAD += "spidev"

IMAGE_FEATURES += "package-management"
CORE_IMAGE_EXTRA_INSTALL += " kernel-modules"
EXTRA_IMAGE_FEATURES ?= "debug-tweaks dev-pkgs tools-sdk"

#IMAGE_INSTALL += " caribou-mod cmake git subversion python python3 python-numpy nfs-utils sysstat gdbserv gdb i2c-tools nano screen peary eudaq ntp"
IMAGE_INSTALL += " cmake git subversion python python3 nfs-utils sysstat gdb i2c-tools nano screen peary ntp spitools"
IMAGE_INSTALL += "git-perltools"
#IMAGE_INSTALL += "zile"
IMAGE_INSTALL += "python-pip python-json python-periphery"
#IMAGE_INSTALL += "python3-pip packagegroup-python3-jupyter python3-json  python3-periphery"
IMAGE_INSTALL += "go"